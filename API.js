let express = require('express');
// Parametres
let hostname = 'localhost';
let port = 3000;
let app = express();
let request = require('request');

//Afin de faciliter le routage (les URL que nous souhaitons prendre en charge dans notre API), nous créons un objet Router.
//C'est à partir de cet objet myRouter, que nous allons implémenter les méthodes.
let myRouter = express.Router();

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

myRouter.route('/pharmacies')
    .get(function(req,res){
        request({
            headers: {
                'X-API-KEY': 'eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjoiNWNkMTMzZDY4YjRjNDE1ZTljMmI2YTZlIiwidGltZSI6MTU1NzIxNDIwNy41OTM0NjF9.568ApM4G-EENxVzkzuqiJiLg7p8VbEFlz45rbukYshs'
            },
            uri: 'https://opendata.lillemetropole.fr//explore/dataset/pharmacies/download?format=geojson&timezone=Europe/Berlin&use_labels_for_header=true',
            method: 'GET'
        },  (error, result, body)=>{
            if (error){
                throw new Error (err);
            }else{
                console.log(result)
            }
            if(body){
                console.log(body);
                //res.value(body);
                res.json(body);
            }else{
                throw new Error ("no result");
            }

        });
    })
    .post(function(req,res){
        res.json({message : "Ajoute une nouvelle piscine à la liste", methode : req.method});
    })
    .put(function(req,res){
        res.json({message : "Mise à jour des informations d'une piscine dans la liste", methode : req.method});
    })
    .delete(function(req,res){
        res.json({message : "Suppression d'une piscine dans la liste", methode : req.method});
    });



app.use(myRouter);
app.listen(port, hostname, function(){
    console.log("Listen on http://"+ hostname +":"+port);
});
